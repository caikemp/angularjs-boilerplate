AngularJS Template
=

Install
=
```sh
# Install Node Modules
npm install

# Start the Server
npm start

# If you want to edit the angular code, this rebuilds
gulp watch
```

> The server will be available at localhost:5000


# Implementation Details

Here are some details for this template:

In the `app.js` file, you'll configure the node server as port for e.g.


## Controllers
Here is a simple controller. Put it into the src->app->controller folder

```js
angular
  .module(DEFAULT.PKG())
  .controller('SomeCtrl',
  ['$scope', '$rootScope',
    function ($scope, $rootScope) {

    }
  ]);
```

## Services

```js
/*
 * The Service
 */
angular
  .module(DEFAULT.PKG('game'), [

    /*
     * Angular framework dependencies
     */
    'ngStorage',

    /*
     * Project dependencies
     */
    DEFAULT.PKG('api')

  ])

  /*
   * The service factory
   */
  .factory('$serviceName',

  /*
   * Module dependencies
   */
  ['$sessionStorage', '$api',

    /**
     * Service
     *
     * @param $storage    The application $storage interface
     * @param $api        The API interface
     */
      function ($storage, $api) {

      /*
       * The user service public interface
       */
      var p = {


        /**
         * functionName
         *
         * @param {Object} data
         * @param {Function} [callback]
         */
        functionName: function (data, callback) {

          callback = callback || angular.noop;

          $api
            .get('/path/url', data, '')
            .success(function (response) {
              if(response && response.status && response.status === 'SUCCESS') {
                callback(null, response);
              } else {
                callback(response || new Error(DEFAULT.ERROR.UNKNOWN));
              }
            }).error(function (error) {
              callback(error || new Error(DEFAULT.ERROR.UNKNOWN));
            });

        }

      };

      return p;
    }
  ]);
```
