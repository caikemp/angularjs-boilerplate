#!/bin/sh

echo "----------------------------------------";
echo "Installing bower components...";
./node_modules/bower/bin/bower install --config.interactive=false -F

echo "----------------------------------------";
echo "Building files using Grunt...";
./node_modules/grunt-cli/bin/grunt default

echo "----------------------------------------";
echo "Post install script OK!";
echo "----------------------------------------";

exit 0;