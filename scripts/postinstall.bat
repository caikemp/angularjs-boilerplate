REM #!/bin/sh

REM echo "----------------------------------------";
REM echo "Installing bower components...";
bower install --config.interactive=false -F

REM echo "----------------------------------------";
REM echo "Building files using Grunt...";
./node_modules/grunt-cli/bin/grunt default

REM echo "----------------------------------------";
REM echo "Post install script OK!";
REM echo "----------------------------------------";

REM exit 0;

