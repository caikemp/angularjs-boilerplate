angular.module(DEFAULT.PKG())
    .directive('toUpper', function () {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, modelCtrl) {
                var capitalize = function (inputValue) {
                    if (inputValue == undefined) inputValue = '';
                    var capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                };
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]);
            }

        };
    })
    .directive('isNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (value) {
                    if (value === undefined) return '';
                    var transformedInput = value.replace(/[^0-9]/g, '');
                    if (transformedInput != value) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });

            }
        };
    })
    .directive('isFloat', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (value) {
                    if (value === undefined) return '';
                    var transformedInput = value.replace(/[^0-9,-]/g, '');
                    if (transformedInput != value) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });

            }
        };
    })
    .directive('isAlphanumeric', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (value) {
                    if (value === undefined) return '';
                    var transformedInput = value.replace(/[^a-z0-9]/g, '');
                    if (transformedInput != value) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });

            }
        };
    })
    .directive('isCurrency', function ($filter) {
      //Numbers like: 1.123,56
        var FLOAT_REGEXP_1 = /^\$?\d+.(\d{3})*(\,\d*)$/;
        //Numbers like: 1,123.56
        var FLOAT_REGEXP_2 = /^\$?\d+,(\d{3})*(\.\d*)$/;
        //Numbers like: 1123.56
        var FLOAT_REGEXP_3 = /^\$?\d+(\.\d*)?$/;
        //Numbers like: 1123,56
        var FLOAT_REGEXP_4 = /^\$?\d+(\,\d*)?$/;

        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$parsers.unshift(function (viewValue) {
                    if (FLOAT_REGEXP_1.test(viewValue)) {
                        ctrl.$setValidity('float', true);
                        return parseFloat(viewValue.replace('.', '').replace(',', '.'));
                    } else if (FLOAT_REGEXP_2.test(viewValue)) {
                        ctrl.$setValidity('float', true);
                        return parseFloat(viewValue.replace(',', ''));
                    } else if (FLOAT_REGEXP_3.test(viewValue)) {
                        ctrl.$setValidity('float', true);
                        return parseFloat(viewValue);
                    } else if (FLOAT_REGEXP_4.test(viewValue)) {
                        ctrl.$setValidity('float', true);
                        return parseFloat(viewValue.replace(',', '.'));
                    } else if( ctrl.$isEmpty(viewValue)){
                        ctrl.$setValidity('float', true);
                        return '';
                    } else {
                        ctrl.$setValidity('float', false);
                        return undefined;
                    }

                    //if (FLOAT_REGEXP_3.test(viewValue)) {
                    //    ctrl.$setValidity('float', true);
                    //    return parseFloat(viewValue);
                    //} else {
                    //    ctrl.$setValidity('float', false);
                    //    return undefined;
                    //}
                });

                ctrl.$formatters.unshift(
                    function (modelValue) {
                        return $filter('number')(parseFloat(modelValue), 2);
                    }
                );
            }
        };
    });
