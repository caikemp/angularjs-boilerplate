angular.module(DEFAULT.PKG(), [
    'ngRoute',
    'ngTouch',
    'ngSanitize',
    DEFAULT.PKG('sidebar')
    ])

    .config(function ($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: './app/views/home.html',
                controller: 'HomeCtrl'
            });
    })
    .controller('appCtrl', [

      '$scope', '$rootScope', '$location', '$sidebar',

    function ($scope, $rootScope, $location, $sidebar) {

        $scope.Sidebar = $sidebar;


        $rootScope.app = {version: DEFAULT.APP.version};

        $scope.back = function() {
          $window.history.back();
        };

        $scope.showToolbar = function () {
            if ($location.path() == '/login'  ||
                $location.path() == '/') {
                return false;
            }
            return true;
        };

        $rootScope.path = function (route) {
            return $location.path(route);
        };

    }]);
