angular
    .module(DEFAULT.PKG('sidebar'), [])
    .factory('$sidebar', function ($rootScope) {
        var sidebarIsShown = false;

        function toggleSidebar() {
            sidebarIsShown = !sidebarIsShown;
            if(sidebarIsShown){
              $rootScope.scroolControl = 'no-scrool';
            }else{
              $rootScope.scroolControl = 'scrool';
            }
        }

        function hideSidebar() {
            $rootScope.scroolControl = 'scrool';
            sidebarIsShown = false;
        }

        function showSidebar() {
            $rootScope.scroolControl = 'no-scrool';
            sidebarIsShown = true;
        }

        return {
            isSidebarShown: function () {
                return sidebarIsShown;
            },
            toggleSidebar: toggleSidebar,
            hideSidebar: hideSidebar,
            showSidebar: showSidebar
        };
    });
