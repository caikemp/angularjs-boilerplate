angular
    .module(DEFAULT.PKG('api'), [])
    .factory('$api', ['$http',

        function ($http) {

            var request = function (method, action, input, headers) {

                Array.prototype.contains = function (needle) {
                    for (var i in this) {
                        if (this[i] == needle) return true;
                    }
                    return false;
                };
                
                if(headers != 'integration'){
                  action = (DEFAULT.API.BASE_URL + action);
                }

                return $http({
                    method: method,
                    url: action,
                    headers: headers,
                    data: (method.toUpperCase() !== 'GET' ? input : {}),
                    params: (method.toUpperCase() === 'GET' ? input : {})
                });


            };

            return {

                'get': function (action, input, headers) {
                    return request('get', action, input, headers);
                },

                'post': function (action, input, headers) {
                    return request('post', action, input, headers);
                },

                'put': function (action, input, headers) {
                    return request('put', action, input, headers);
                },

                'delete': function (action, input, headers) {
                    return request('delete', action, input, headers);
                }

            }

        }
    ]);
