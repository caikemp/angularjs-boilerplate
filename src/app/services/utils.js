/*
 * The file
 */
angular
    .module(DEFAULT.PKG('utils'), [])
    .service('$utils',
    ['$window',
        function ($window) {

            var p = {

                /**
                 * Given the position in the sequence of pagination links [i], figure out what page number corresponds to that position.
                 *
                 * @param i
                 * @param currentPage
                 * @param paginationRange
                 * @param totalPages
                 * @returns {*}
                 */
                calculatePageNumber: function (i, currentPage, paginationRange, totalPages) {
                    var halfWay = Math.ceil(paginationRange / 2);
                    if (i === paginationRange) {
                        return totalPages;
                    } else if (i === 1) {
                        return i;
                    } else if (paginationRange < totalPages) {
                        if (totalPages - halfWay < currentPage) {
                            return totalPages - paginationRange + i;
                        } else if (halfWay < currentPage) {
                            return currentPage - halfWay + i;
                        } else {
                            return i;
                        }
                    } else {
                        return i;
                    }
                },

                /**
                 * Generate an array of page numbers (or the '...' string) which is used in an ng-repeat to generate the
                 * links used in pagination
                 *
                 * @param currentPage
                 * @param paginationRange
                 * @param collectionLength
                 * @returns {Array}
                 */
                generatePagesArray: function (currentPage, collectionLength, paginationRange) {
                    var pages = [];
                    var totalPages = collectionLength;
                    var halfWay = Math.ceil(paginationRange / 2);
                    var position;

                    if (currentPage <= halfWay) {
                        position = 'start';
                    } else if (totalPages - halfWay < currentPage) {
                        position = 'end';
                    } else {
                        position = 'middle';
                    }

                    var ellipsesNeeded = paginationRange < totalPages;
                    var i = 1;
                    while (i <= totalPages && i <= paginationRange) {
                        var pageNumber = this.calculatePageNumber(i, currentPage, paginationRange, totalPages);

                        var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
                        var closingEllipsesNeeded = (i === paginationRange - 1 && (position === 'middle' || position === 'start'));
                        if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                            pages.push('...');
                        } else {
                            pages.push(pageNumber);
                        }
                        i++;
                    }
                    return pages;
                },

                checkBrowser: function () {
                    var userAgent = $window.navigator.userAgent;
                    var browsers = {
                        chrome: /chrome/i,
                        safari: /safari/i,
                        firefox: /firefox/i,
                        ie: /internet explorer/i
                    };

                    for (var key in browsers) {
                        if (browsers[key].test(userAgent)) {
                            return key;
                        }
                    }
                    return 'unknown';

                }
            };

            return p;
        }
    ]);
