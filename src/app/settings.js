var DEFAULT = {

  CLIENT_SECRET: '',
  MAIN_PKG: 'br.com.angularboilerplate',
  // GOGLE GCM
  SENDER_ID : '',

  API: {

    /**
     * The API base URL for all requests.
     */
    BASE_URL: '',

  },
  APP: {
    version: "1.0.0"
  },
  PKG: function (suffix) {
    if (suffix && suffix.length) {
      return DEFAULT.MAIN_PKG + '.' + suffix;
    } else {
      return DEFAULT.MAIN_PKG;
    }
  },
  ERROR: {
    UNKNOWN: 'Erro desconhecido'
  }

};
