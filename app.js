

var app, server,
  express = require('express'),
  port = process.env.PORT || 5001,
  path = require('path').join(__dirname, './src');

app = express();


app.use(require('mean-seo')({
  cacheClient: 'disk', // Can be 'disk' or 'redis'
  cacheDuration: 24 * 1000 // In milliseconds for disk cache
}));

app.use(express.static(path));

server = app.listen(port, serverStarted);

function serverStarted() {
  console.log('Server started at port: http://localhost:'+  port);
  console.log('Press Ctrl+C to stop...\n');
}
