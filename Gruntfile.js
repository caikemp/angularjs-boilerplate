module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: ['src/assets/less']
                },
                files: {
                    'src/dist/style.css': 'src/assets/less/**.less'
                }
            }
        },
        uglify: {
            my_target: {
                options: {
                    mangle: false
                },
                files: {
                    'src/app.min.js': [
                        './src/app/settings.js',
                        './src/app/main.js',
                        './src/app/controllers/**.js',
                        './src/app/directives/**.js',
                        './src/app/services/**.js'
                    ]
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'src/dist/style.min.css': ['src/dist/style.css']
                }
            }
        },
        clean: ["src/assets/dist/"],
        json: {
            mock: {
                options: {
                    namespace: 'mock'
                },
                src: ['src/app/data/mock_*.json'],
                dest: 'tests/mock/json.js'
            }
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                options: {
                    autoWatch: false,
                    singleRun: true
                }
            }
        },
        watch: {
            files: [
                './src/app/settings.js',
                './src/app/main.js',
                './src/app/controllers/**',
                './src/app/data/**',
                './src/app/services/**',
                './src/app/directives/**',
                './src/app/views/**',
                './src/assets/**',
                './src/index.html'
            ],
            tasks: ['dev']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-json');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('dev', ['clean', 'less', 'cssmin', 'uglify']);
    grunt.registerTask('test', ['json', 'karma']);
    grunt.registerTask('default', ['dev', 'test']);
};
